cd %~dp0

REM Build within a Linux Container to get the CR/LF line endings to be correct when building the Linux Container Image
call docker_image_builder\docker-compose.linux.build.bat

cd %~dp0
docker-compose -f docker-compose.yml up --scale minion=2